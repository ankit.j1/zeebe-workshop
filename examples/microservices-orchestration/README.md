# Microservices Orchestration

This example showcases how Zeebe could orchestrate a payment microservice from within an order fulfillment microservice when Kafka is used as transport.

![Process](order-microservices-orchestration.png)


> The `Logger` tasks are there only demo purposes to follow along on the console.

It leverages the connector's source to push a message onto Kafka whenever a payment is required, expecting some payment service to process it and emit an event (or send a response message) later on. This response is correlated to the waiting order fulfillment process using the connectors sink.

The example needs the payment service to be simulated, means you need to publish a record to the `payment-confirm` topic. You could do that using the [kafka-console-producer](https://kafka.apache.org/quickstart#quickstart_send):

```json
{
  "eventType": "OrderPaid",
  "orderId": 1,
  "amount": 4000
}
```
