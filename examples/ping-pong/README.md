# Ping Pong

This is a very simple example to showcase the interaction between Zeebe and Kafka using Kafka Connect and the Zeebe source and sink connectors, using the following process:

![Process](process.png)

When an instance of this process is created, the service task `To Kafka` will be activated by the
source connector `ping`, which will publish a record on the topic `pong`. That record will have
the job key as its key, and as value the job itself serialized to JSON.

The `pong` sink connector will consume records from the `pong` topic, and publish a message to
its Zeebe broker. For example, given the following record published to `pong` on partition 1,
at offset 1, and with the following value:

```json
{
  "name": "pong",
  "key": 1,
  "payload": {
    "foo": "bar"
  },
  "ttl": 10000,
  ...
}
```

It will publish the following message to Zeebe:

```json
{
  "name": "pong",
  "correlationKey": 1,
  "timeToLive": 10000,
  "messageId": "pong:1:1",
  "variables": {
    "foo": "bar"
  }
}
```
